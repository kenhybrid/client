import { createStore } from "vuex";
import axios from "../plugins/axios.js";
import createPersistedState from "vuex-persistedstate";
export default createStore({
  plugins: [
    createPersistedState({
      key: "shopper",
    }),
  ],
  state: {
    products: [],
    pagination: {
      pages: "",
      next: {
        page: "",
      },
      previous: {
        page: "",
      },
      count: "",
      limit: "",
    },
  },
  getters: {
    getProducts(state) {
      return state.products;
    },
    getPage(state) {
      return state.pagination.next.page;
    },
    getPages(state) {
      return state.pagination.pages;
    },
  },
  mutations: {
    setProducts(state, payload) {
      this.state.products = payload;
    },
    setPagination(state, payload) {
      this.state.pagination = payload;
    },
    setPaginatedProducts(state, payload) {
      this.state.products = [...this.state.products, ...payload];
    },
  },
  actions: {
    async getAllProducts({ commit }) {
      try {
        const chunk = await axios({
          method: "GET",
          url: "/product/all",
        });
        const { doc, metadata } = chunk.data;
        commit("setProducts", doc);
        commit("setPagination", metadata);
      } catch (error) {
        console.log(error);
      }
    },
    async paginateProducts({ commit }, payload) {
      const chunk = await axios({
        method: "GET",
        url: `/product/all?page=${payload.page}`,
      });
      const { doc, metadata } = chunk.data;
      commit("setPaginatedProducts", doc);
      commit("setPagination", metadata);
    },
  },

  modules: {},
});
