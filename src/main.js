import "./plugins/axios";
import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import { createHead } from "@vueuse/head";
import "materialize-css/dist/css/materialize.min.css";
createApp(App).use(store).use(createHead()).use(router).mount("#app");
